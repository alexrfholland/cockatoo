﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Grasshopper.Kernel;
using Rhino.Geometry;

namespace cokatoo
{
    class Co_QSM
    {
        public int
          branchID,
          branch_order,
          segmentID,
          parent_segment_ID;

        public double
            growth_volume,
            growth_length;

        public string
            detection,
            improvment;

        public double
            startX,
            startY,
            startZ,

            endX,
            endY,
            endZ,

            radius,
            length,

            length_to_leave,
            inverse_branch_order,
            length_of_segment,
            branch_order_cum;

        public Co_QSM(
            int _branchID,
            int _branch_order,
            int _segmentID,
            int _parent_segment_ID,

            double _growth_volume,
            double _growth_length,
            string _detection,
            string _improvment,

            double _startX,
            double _startY,
            double _startZ,

            double _endX,
            double _endY,
            double _endZ,

            double _radius,
            double _length,

            double _length_to_leave,
            double _inverse_branch_order,
            double _length_of_segment,
            double _branch_order_cum)
        {
            branchID = _branchID;
            branch_order = _branch_order;
            segmentID = _segmentID;
            parent_segment_ID = _parent_segment_ID;

            growth_volume = _growth_volume;
            growth_length = _growth_length;

            detection = _detection;
            improvment = _improvment;

            startX = _startX;
            startY = _startY;
            startZ = _startZ;

            endX = _endX;
            endY = _endY;
            endZ = _endZ;

            radius = _radius;
            length = _length;

            length_to_leave = _length_to_leave;
            inverse_branch_order = _inverse_branch_order;
            length_of_segment = _length_of_segment;
            branch_order_cum = _branch_order_cum;
        }

        public Co_QSM (int _segmentID) 
            {
            segmentID = _segmentID;
        }
    }

    class Co_Tree
    {
        public Dictionary<int, Co_Segment> segments;
        List<Co_QSM> qsmData;

        public Co_Tree (List<Co_QSM> _qsmData)
        {
            LoadSegments();

            foreach ( Co_Segment segment in segments.Values)
            {
                segment.GetParents(segments);
            }
        }

        public Dictionary<int, Co_Segment> LoadSegments()
        {
            Dictionary<int, Co_Segment> _segments = new Dictionary<int, Co_Segment>();

            foreach (Co_QSM qsmLine in qsmData)
            {
                segments.Add(qsmLine.segmentID, new Co_Segment(qsmLine));
            }

            return _segments;
        }
    }

    class Co_Segment
    {
        public Co_QSM qsm;

        public Vector3d start, end;
        public double radius, length;
        public Co_Segment parentSegment;

        public Co_Segment(Co_QSM _qsm)
        {
            qsm = _qsm;
            start = new Vector3d(qsm.startX, qsm.startY, qsm.startZ);
            end = new Vector3d(qsm.endX, qsm.endY, qsm.endZ);
            radius = qsm.radius;
            length = qsm.length;
        }

        public void GetParents(Dictionary<int, Co_Segment> segments)
        {
            parentSegment = segments[qsm.parent_segment_ID];
        }

    }

    class Co_Leaves
    {
        PointCloud leafCloud;
        double spacing;

        public Co_Leaves (PointCloud _leafCloud)
        {
            leafCloud = new PointCloud(_leafCloud);

            FindSpacing();
        }

        void FindSpacing()
        {
            double smallestDist = 9999999;
            
            foreach (PointCloudItem pt in leafCloud)
            {
                double dist = leafCloud.PointAt(0).DistanceToSquared(pt.Location);
                if (smallestDist < dist)
                {
                    smallestDist = dist;
                }
            }

            spacing = smallestDist;
        }
    }
}

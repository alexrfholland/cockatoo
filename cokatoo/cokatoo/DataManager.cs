﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System;
using DeepDesignLab.Base;

namespace cokatoo {
    class DataManager 
        {
        static int fileReaderProgress = -1; 

        List<Co_QsmReader> QsmReaders = new List<Co_QsmReader>();
        List<Co_QSM> QSMFiles = new List<Co_QSM>();
        List<Co_Tree> trees = new List<Co_Tree>();

        string folderPath = "C:\\Users\\julia\\The University of Melbourne\\Alexander Holland - _Source data\\Trees\\Scanning\\Canberra\\15-2 s1Tu-kh1\\QSM Trunk\\detailed";
        string[] filePaths;

        // output variables...
        public Co_Tree outputTree {
            get {
                if (fileReaderProgress == 2) {
                    return outputTree;
                }
                else {
                    return null;
                }
            }
            private set { }
        }

        public void Loop () 
        {
            if(fileReaderProgress == -1) 
                {

                fileReaderProgress = 0;
                filePaths = Directory.GetFiles(folderPath);
                foreach (string path in filePaths) 
                    {
                    Co_QsmReader tempRead = new Co_QsmReader();
                    QsmReaders.Add(tempRead);
                    tempRead.readFile(path);
                }
            }


            if (fileReaderProgress == 0) {
                bool hasFinishedAllThreads = false;
                foreach (var item in QsmReaders) {
                    if (!item.hasFinished) {
                        hasFinishedAllThreads = false;
                        break;
                    }
                    else {
                        hasFinishedAllThreads = true;
                    }
                }
                if (hasFinishedAllThreads == true) {
                    fileReaderProgress = 1;
                }
            }

            if (fileReaderProgress == 1) 
                {

                foreach (Co_QsmReader reader in QsmReaders)
                {

                }
                
                //next step turn into branch objects
                // create tree
                outputTree = new Co_Tree(QSMFile1);
                fileReaderProgress = 2;
            }

            if (fileReaderProgress == 2) 
                {
                //outputs for grasshopper
            }

        }
        

    }
}
